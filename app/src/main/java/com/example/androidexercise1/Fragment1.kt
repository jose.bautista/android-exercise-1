package com.example.androidexercise1
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class Fragment1 : Fragment(R.layout.fragment_1) {
    lateinit var myViewModel : ActivityViewModel


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        //super.onViewCreated(view, savedInstanceState)

        myViewModel = ViewModelProvider(this).get(ActivityViewModel::class.java)

        val rcv = view.findViewById<RecyclerView>(R.id.recycle)
        rcv?.adapter = ProjectAdapter(requireContext(), myViewModel.projects)


        {
            findNavController().navigate(Fragment1Directions.actionFragment1ToFragment2(it.long_description, it.big_Image))
        }



    }

}