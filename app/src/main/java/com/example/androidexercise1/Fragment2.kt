package com.example.androidexercise1
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavArgs
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.card.view.*
import kotlinx.android.synthetic.main.fragment_2.view.*


class Fragment2 : Fragment() {

    private val args: Fragment2Args by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_2, container, false)
        val long_desc = args.description
        val img = args.image


        view.imageView.setImageResource(img)
        view.textView1.text = long_desc









        return view
    }
}