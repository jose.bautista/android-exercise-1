package com.example.androidexercise1

data class Projects(
    val image: Int,
    val title: String,
    val desc: String,
    val long_description: String,
    val big_Image : Int


)
