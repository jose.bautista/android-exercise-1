package com.example.androidexercise1

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView


class ProjectAdapter(val context: Context, private val proj: List<Projects>, val onItemClicked: (Projects) -> Unit) : RecyclerView.Adapter<ProjectAdapter.myViewHolder>(){



    inner class myViewHolder(view: View): RecyclerView.ViewHolder(view){

        private val title = view.findViewById<TextView>(R.id.Title)
        private val description = view.findViewById<TextView>(R.id.description)
        private val image = view.findViewById<ImageView>(R.id.imageView2)
        private val root by lazy {view.rootView}

        fun bindTitle(word: String) {
            title.text = word
        }

        fun bindDescription(word: String) {
            description.text = word
        }

        fun bindImage(ref: Int){
            image.setImageResource(ref)

        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): myViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card, parent, false)

        return myViewHolder(view)



    }

    override fun onBindViewHolder(holder: myViewHolder, position: Int) {

        holder.bindTitle(proj[position].title)
        holder.bindDescription(proj[position].desc)
        holder.bindImage(proj[position].image)

        holder.itemView.setOnClickListener{
            onItemClicked(proj[position])
        }




    }

    override fun getItemCount(): Int {
        return proj.size
    }
}