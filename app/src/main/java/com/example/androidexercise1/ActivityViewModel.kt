package com.example.androidexercise1

import androidx.lifecycle.ViewModel

class ActivityViewModel : ViewModel() {

    val projects = listOf(
        Projects(
            image = R.drawable.bulbasaur_icon,
            title = "フシギダネ",
            desc = "Bulbasaur - Grass Pokemon",
            long_description =  "Bulbasaur (フシギダネ Fushigidane) is a Grass/Poison-type Seed Pokémon and is the initial Pokémon in the National Pokédex, numbered #001. Bulbasaur evolves into Ivysaur at level 16 then Venusaur at level 32",
            big_Image = R.drawable.bulbasaur
        ),
        Projects(
            big_Image = R.drawable.mudkip,
            image = R.drawable.mudkip_icon,
            title = "ミズゴロウ",
            desc = "Mudkip - Water Pokemon",
            long_description =  "Mudkip (Japanese: ミズゴロウ Mizugorou) is a Water-type Pokémon introduced in Generation III. It evolves into Marshtomp starting at level 16, which evolves into Swampert starting at level 36."


        ),
        Projects(
            big_Image = R.drawable.squirtle,
            image = R.drawable.squirtle_icon,
            title = "ゼニガメ",
            desc = "Squirtle - Water Pokemon",
            long_description =  "Squirtle (Japanese: ゼニガメ Zenigame) is a Water-type Pokémon introduced in Generation I.It evolves into Wartortle starting at level 16, which evolves into Blastoise starting at level 36."
        )
    )




}